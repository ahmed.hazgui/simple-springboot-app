FROM openjdk:11-jdk-slim-stretch

WORKDIR /simple-springboot-app

COPY target/demo-0.0.1-SNAPSHOT.jar .

ENTRYPOINT ["java", "-jar", "demo-0.0.1-SNAPSHOT.jar"]